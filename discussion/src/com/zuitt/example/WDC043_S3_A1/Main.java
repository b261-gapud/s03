package com.zuitt.example.WDC043_S3_A1;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);


        System.out.println("Input an integer whose factorial will be computed:");

        try {

            int num = in.nextInt();

            int x = 1;
            for (int i = 1; i <= num; i++) {

                x = x * i;
            }
            System.out.println("The factorial of " + num + " is " + x);

        }catch (Exception e){
            System.out.println("Invalid input");
            e.printStackTrace();
        }







    }

}
